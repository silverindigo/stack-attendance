import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
const styles = theme => ({
  form: {
    display: "flex",
    alignItems: "center"
  },
  textField: {
    flex: 1,
    marginRight: theme.spacing.unit * 2
  }
});
const CommentForm = props => {
  const { classes, handleChange, handleSubmit } = props;
  return (
    <form onSubmit={handleSubmit} className={classes.form}>
      <TextField
        type="text"
        multiline
        defaultValue=""
        autoFocus
        onChange={handleChange}
        className={classes.textField}
        inputProps={{ style: { maxHeight: 400 } }}
      />
      <Button type="submit">Add</Button>
    </form>
  );
};

CommentForm.protoTypes = {
  classes: PropTypes.object.isRequired,
  handleChange: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired
};

export default withStyles(styles)(CommentForm);

import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableFooter from "@material-ui/core/TableFooter";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import Modal from "@material-ui/core/Modal";
import { ContextMenu, MenuItem, ContextMenuTrigger } from "react-contextmenu";
import moment from "moment";

import { getDates } from "../../utils/datesUtil";
import {
  PRESENT,
  ABSENT,
  EARLY_DEPARTURE,
  LATE,
  SYMBOLS as ATTENDANCE_SYMBOLS
} from "../../constants/attendance";
import CommentForm from "./CommentForm";
const styles = theme => ({
  root: {},
  table: {
    minWidth: 650
  },
  tableCell: {
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2,
    paddingLeft: theme.spacing.unit * 3.1,
    paddingRight: theme.spacing.unit * 4,
    margin: 0,
    boxShadow: "1px 0px 1px #ddd inset",
    fontSize: "13.333px"
  },
  nameTableCell: {
    minWidth: 100
  },
  tableWeekDayCell: {
    width: 95,
    boxSizing: "border-box"
  },
  tableHead: {
    backgroundColor: "#DDD",
    borderTop: "1px solid #000",
    borderBottom: "1px solid #000"
  },
  tableRow: {
    "& th:first-child, & td:first-child": {
      paddingLeft: theme.spacing.unit * 4
    },
    "& th:last-child, & td:last-child": {
      paddingRight: theme.spacing.unit * 4
    }
  },
  footerTableCell: {
    color: "#000",
    fontSize: "0.8125rem",
    borderBottom: "1px solid rgba(224, 224, 224, 1)"
  },
  tableFooter: { borderTop: "3px solid black" },
  relativePosition: {
    position: "relative"
  },
  cursorPointer: {
    cursor: "pointer",
    "&:hover": {
      backgroundColor: "#C9C9C9"
    }
  },
  contextMenuTrigger: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center"
  },
  highlightCell: {
    backgroundColor: "#F2F2F2",
    "&:hover, &:focus, &:active": {
      boxShadow: "0px 0px 1px 1px #000 inset"
    }
  },
  tableHeadDate: {
    border: "1px solid transparent",
    borderRadius: "50%",
    padding: theme.spacing.unit * 0.5,
    boxSizing: "border-box",
    width: 26,
    height: 26,
    display: "block",
    margin: "auto"
  },
  highlightDate: {
    borderColor: "red"
  },
  hasCommentButton: {
    backgroundColor: "red",
    width: 8,
    height: 8,
    display: "block",
    position: "absolute",
    top: 0,
    right: 0,
    cursor: "pointer"
  },
  modalPaper: {
    position: "absolute",
    width: 400,
    padding: theme.spacing.unit * 4,
    left: "50%",
    top: "50%",
    transform: "translate(-50%,-50%)"
  },
  modalContent: {
    padding: theme.spacing.unit,
    maxHeight: 400,
    overflowY: "auto"
  }
});

class AttendanceTable extends Component {
  state = {
    modalOpen: false,
    modalTitle: "Comment",
    modalContent: "",
    comment: ""
  };
  componentDidMount() {
    const { getStudentsDataAction } = this.props;
    getStudentsDataAction();
  }
  handleContextMenuClick = (e, data) => {
    if (typeof data.action === "function" && Array.isArray(data.actionArgs)) {
      data.action(...data.actionArgs);
    } else if (
      typeof data.action === "function" &&
      data.actionArgs === undefined
    ) {
      data.action();
    } else if (data.action === "ADD_COMMENT_MODAL") {
      this.setState({
        modalContent: this.renderCommentTextField(data.actionArgs),
        modalOpen: true
      });
    }
  };

  handleDateClick = activeDateIndex => {
    const { setCalendarDateAction } = this.props;
    const dates = this.getDates();

    setCalendarDateAction({
      activeDate: dates[activeDateIndex].dateObject
    });
  };

  getDates = () => {
    const { currentDate, view } = this.props;

    // next 6 days
    return getDates(currentDate, view).filter(
      // exclude saturday & sunday
      date => date.shortDay !== "Sat" && date.shortDay !== "Sun"
    );
  };

  dateEqual = (date1, date2) => moment(date1).isSame(date2, "day");

  toggleModal = () => this.setState({ modalOpen: !this.state.modalOpen });

  renderCommentTextField = actionArgs => {
    const { addCommentAction } = this.props;
    return (
      <CommentForm
        handleChange={e => this.setState({ comment: e.target.value }, () => {})}
        handleSubmit={e => {
          e.preventDefault();
          this.handleContextMenuClick(e, {
            actionArgs: [...actionArgs, this.state.comment],
            action: addCommentAction
          });
          this.toggleModal();
        }}
      />
    );
  };
  renderCurrentSymbolMark = (student, date) => {
    const attendanceType = student.attendance[date];
    return attendanceType ? ATTENDANCE_SYMBOLS[attendanceType] : null;
  };
  renderStudentAttendanceCount = (student, attendanceType) => {
    let count = 0;
    for (let key in student.attendance) {
      const type = student.attendance[key];
      if (type === attendanceType) count++;
    }
    return count;
  };
  renderStudentAttendanceTally = (attendanceType, date) => {
    const { students, activeDate } = this.props;
    let count = 0;
    for (let student of students) {
      if (student.attendance[date] === attendanceType) count++;
    }
    return this.dateEqual(activeDate, date) ? count : null;
  };
  renderComment = (student, date) => {
    const { classes } = this.props;
    const { deleteCommentAction } = this.props;
    const comment = student.comments[date];
    const uniqueId = `${student.id}_${Number(
      date
    )}student_attendance_comment_menus`;
    if (comment) {
      return (
        <React.Fragment key={uniqueId}>
          <ContextMenuTrigger
            id={uniqueId}
            attributes={{
              className: classes.hasCommentButton,
              onClick: () => {
                this.setState({ modalContent: comment }, () => {
                  this.toggleModal();
                });
              }
            }}
            renderTag={"span"}
          >
            {" "}
          </ContextMenuTrigger>

          <ContextMenu id={uniqueId}>
            <MenuItem
              data={{
                actionArgs: [student.id, date],
                action: deleteCommentAction
              }}
              onClick={this.handleContextMenuClick}
            >
              Delete
            </MenuItem>
          </ContextMenu>
        </React.Fragment>
      );
    } else {
      return null;
    }
  };

  render() {
    const {
      classes,
      students,
      markAttendanceAction,
      markAllAttendanceAction,
      unmarkAttendanceAction,
      unmarkAllAttendanceAction,

      activeDate
    } = this.props;
    const dates = this.getDates();
    return (
      <div className={classes.root}>
        <Table className={classes.table}>
          <TableHead>
            <TableRow className={classes.tableRow}>
              <TableCell
                align="center"
                className={[classes.tableCell, classes.tableHead].join(" ")}
              >
                No.
              </TableCell>
              <TableCell
                align="center"
                className={[
                  classes.tableCell,
                  classes.tableHead,
                  classes.nameTableCell
                ].join(" ")}
              >
                Name
              </TableCell>
              <TableCell
                align="center"
                className={[classes.tableCell, classes.tableHead].join(" ")}
              >
                P
              </TableCell>
              <TableCell
                align="center"
                className={[classes.tableCell, classes.tableHead].join(" ")}
              >
                A
              </TableCell>
              <TableCell
                align="center"
                className={[classes.tableCell, classes.tableHead].join(" ")}
              >
                ED
              </TableCell>
              <TableCell
                align="center"
                className={[classes.tableCell, classes.tableHead].join(" ")}
              >
                L
              </TableCell>
              {dates.map((date, i) => (
                <TableCell
                  align="center"
                  key={Number(date.dateObject)}
                  className={[
                    classes.tableHead,
                    classes.tableCell,
                    classes.cursorPointer,
                    classes.tableWeekDayCell
                  ].join(" ")}
                  onClick={() => this.handleDateClick(i)}
                >
                  {date.shortDay}
                  <br />
                  <br />
                  <span
                    className={[
                      this.dateEqual(activeDate, date.dateObject)
                        ? classes.highlightDate
                        : null,
                      classes.tableHeadDate
                    ].join(" ")}
                  >
                    {date.date}
                  </span>
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {students.map(student => (
              <TableRow className={classes.tableRow} key={student.id}>
                <TableCell
                  component="th"
                  scope="row"
                  align="center"
                  className={classes.tableCell}
                >
                  {student.id}
                </TableCell>
                <TableCell
                  align="left"
                  className={[classes.tableCell, classes.nameTableCell].join(
                    " "
                  )}
                >
                  {student.name}
                </TableCell>
                <TableCell align="center" className={classes.tableCell}>
                  {this.renderStudentAttendanceCount(student, PRESENT)}
                </TableCell>
                <TableCell align="center" className={classes.tableCell}>
                  {this.renderStudentAttendanceCount(student, ABSENT)}
                </TableCell>
                <TableCell align="center" className={classes.tableCell}>
                  {this.renderStudentAttendanceCount(student, EARLY_DEPARTURE)}
                </TableCell>
                <TableCell align="center" className={classes.tableCell}>
                  {this.renderStudentAttendanceCount(student, LATE)}
                </TableCell>

                {dates.map(d =>
                  this.dateEqual(activeDate, d.dateObject) ? (
                    <ContextMenuTrigger
                      key={Number(d.dateObject)}
                      id={`${student.id}_student_attendance_mark_menus`}
                      attributes={{
                        className: [
                          classes.tableCell,
                          classes.tableWeekDayCell,
                          classes.highlightCell,
                          classes.contextMenuCell,
                          classes.relativePosition
                        ].join(" ")
                      }}
                      renderTag={TableCell}
                    >
                      {this.renderCurrentSymbolMark(student, d.dateObject)}
                      {this.renderComment(student, d.dateObject)}
                    </ContextMenuTrigger>
                  ) : (
                    <TableCell
                      align="center"
                      key={Number(d.dateObject)}
                      className={[
                        classes.tableCell,
                        classes.tableWeekDayCell,
                        classes.relativePosition
                      ].join(" ")}
                    >
                      {this.renderCurrentSymbolMark(student, d.dateObject)}
                      {this.renderComment(student, d.dateObject)}
                    </TableCell>
                  )
                )}
              </TableRow>
            ))}
          </TableBody>
          <TableFooter className={classes.tableFooter}>
            <TableRow className={classes.tableRow}>
              <TableCell
                className={[classes.tableCell, classes.footerTableCell].join(
                  " "
                )}
              />
              <TableCell
                align="left"
                className={[classes.tableCell, classes.footerTableCell].join(
                  " "
                )}
                colSpan={5}
              >
                Absent
              </TableCell>
              {dates.map(d => (
                <TableCell
                  key={Number(d.dateObject)}
                  align="center"
                  className={[
                    classes.tableCell,
                    classes.footerTableCell,
                    this.dateEqual(activeDate, d.dateObject)
                      ? classes.highlightCell
                      : null
                  ].join(" ")}
                >
                  {this.renderStudentAttendanceTally(ABSENT, d.dateObject)}
                </TableCell>
              ))}
            </TableRow>
            <TableRow className={classes.tableRow}>
              <TableCell
                className={[classes.tableCell, classes.footerTableCell].join(
                  " "
                )}
              />
              <TableCell
                align="left"
                className={[classes.tableCell, classes.footerTableCell].join(
                  " "
                )}
                colSpan={5}
              >
                Early Departure
              </TableCell>
              {dates.map(d => (
                <TableCell
                  key={Number(d.dateObject)}
                  align="center"
                  className={[
                    classes.tableCell,
                    classes.footerTableCell,
                    this.dateEqual(activeDate, d.dateObject)
                      ? classes.highlightCell
                      : null
                  ].join(" ")}
                >
                  {this.renderStudentAttendanceTally(
                    EARLY_DEPARTURE,
                    d.dateObject
                  )}
                </TableCell>
              ))}
            </TableRow>
            <TableRow className={classes.tableRow}>
              <TableCell
                className={[classes.tableCell, classes.footerTableCell].join(
                  " "
                )}
              />
              <TableCell
                align="left"
                className={[classes.tableCell, classes.footerTableCell].join(
                  " "
                )}
                colSpan={5}
              >
                Late
              </TableCell>
              {dates.map(d => (
                <TableCell
                  key={Number(d.dateObject)}
                  align="center"
                  className={[
                    classes.tableCell,
                    classes.footerTableCell,
                    this.dateEqual(activeDate, d.dateObject)
                      ? classes.highlightCell
                      : null
                  ].join(" ")}
                >
                  {this.renderStudentAttendanceTally(LATE, d.dateObject)}
                </TableCell>
              ))}
            </TableRow>
            <TableRow className={classes.tableRow}>
              <TableCell
                className={[classes.tableCell, classes.footerTableCell].join(
                  " "
                )}
              />
              <TableCell
                align="left"
                className={[classes.tableCell, classes.footerTableCell].join(
                  " "
                )}
                colSpan={5}
              >
                Present
              </TableCell>
              {dates.map(d => (
                <TableCell
                  key={Number(d.dateObject)}
                  align="center"
                  className={[
                    classes.tableCell,
                    classes.footerTableCell,
                    this.dateEqual(activeDate, d.dateObject)
                      ? classes.highlightCell
                      : null
                  ].join(" ")}
                >
                  {this.renderStudentAttendanceTally(PRESENT, d.dateObject)}
                </TableCell>
              ))}
            </TableRow>
          </TableFooter>
        </Table>
        {students.map(student => (
          <ContextMenu
            key={student.id}
            id={`${student.id}_student_attendance_mark_menus`}
          >
            <MenuItem
              data={{
                actionArgs: [activeDate, PRESENT],
                action: markAllAttendanceAction
              }}
              onClick={this.handleContextMenuClick}
            >
              Present (All)
            </MenuItem>
            <MenuItem
              data={{
                actionArgs: [student.id, activeDate, ABSENT],
                action: markAttendanceAction
              }}
              onClick={this.handleContextMenuClick}
            >
              Absent
            </MenuItem>
            <MenuItem
              data={{
                actionArgs: [student.id, activeDate, EARLY_DEPARTURE],
                action: markAttendanceAction
              }}
              onClick={this.handleContextMenuClick}
            >
              Early Departure
            </MenuItem>
            <MenuItem
              data={{
                actionArgs: [student.id, activeDate, LATE],
                action: markAttendanceAction
              }}
              onClick={this.handleContextMenuClick}
            >
              Late
            </MenuItem>
            <MenuItem
              data={{
                actionArgs: [student.id, activeDate],
                action: "ADD_COMMENT_MODAL"
              }}
              onClick={this.handleContextMenuClick}
            >
              Comment
            </MenuItem>
            <MenuItem
              data={{
                actionArgs: [student.id, activeDate, PRESENT],
                action: markAttendanceAction
              }}
              onClick={this.handleContextMenuClick}
            >
              Present (This cell)
            </MenuItem>
            <MenuItem divider />
            <MenuItem
              data={{
                actionArgs: [student.id, activeDate],
                action: unmarkAttendanceAction
              }}
              onClick={this.handleContextMenuClick}
            >
              Clear (This cell)
            </MenuItem>
            <MenuItem
              data={{
                actionArgs: [activeDate],
                action: unmarkAllAttendanceAction
              }}
              onClick={this.handleContextMenuClick}
            >
              Clear (All)
            </MenuItem>
          </ContextMenu>
        ))}

        <Modal
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
          open={this.state.modalOpen}
          onClose={this.toggleModal}
        >
          <Paper className={classes.modalPaper}>
            <Typography variant="h6" id="modal-title" gutterBottom>
              {this.state.modalTitle}
            </Typography>
            {typeof this.state.modalContent === "string" ? (
              <Typography
                variant="subtitle1"
                paragraph
                id="simple-modal-description"
                className={classes.modalContent}
              >
                {this.state.modalContent}
              </Typography>
            ) : (
              this.state.modalContent
            )}
          </Paper>
        </Modal>
      </div>
    );
  }
}

AttendanceTable.propTypes = {
  view: PropTypes.string.isRequired,
  students: PropTypes.array.isRequired,
  getStudentsDataAction: PropTypes.func.isRequired,
  markAttendanceAction: PropTypes.func.isRequired,
  markAllAttendanceAction: PropTypes.func.isRequired,
  unmarkAttendanceAction: PropTypes.func.isRequired,
  unmarkAllAttendanceAction: PropTypes.func.isRequired,
  addCommentAction: PropTypes.func.isRequired,
  deleteCommentAction: PropTypes.func.isRequired,
  setCalendarDateAction: PropTypes.func.isRequired,

  currentDate: PropTypes.object.isRequired,
  activeDate: PropTypes.object.isRequired
};
export default withStyles(styles)(AttendanceTable);

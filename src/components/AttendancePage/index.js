import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import Calendar from "react-calendar";
import AttendanceTable from "./AttendanceTable";
import { WEEKLY_VIEW, MONTHLY_VIEW } from "../..//./constants/views";
import { monthAsString } from "../../utils/datesUtil";
import moment from "moment";
const styles = theme => ({
  root: {
    padding: theme.spacing.unit * 2
  },
  button: {
    marginLeft: theme.spacing.unit
  },
  tableTopItems: { display: "flex" },
  tableNavigation: {
    textAlign: "center",
    flex: 1
  },
  tableNavTitle: {
    minWidth: 153,
    textAlign: "center",
    display: "inline-block"
  },
  viewButtons: {
    marginLeft: "auto",
    marginBottom: theme.spacing.unit * 2
  },
  tableWrapper: {
    overflow: "auto",
    padding: "0 !important"
  },
  calender: {
    margin: "auto"
  }
});
class AttendancePage extends Component {
  state = {
    view: WEEKLY_VIEW
  };
  handleChangeDate = date => {
    const { setCalendarDateAction } = this.props;
    setCalendarDateAction({
      currentDate: date,
      activeDate: date
    });
  };

  handleViewChange = view => {
    this.setState({ view });
  };

  renderTableNavigation = () => {
    const {
      classes,
      currentDate,
      forwardMonthAction,
      backwardMonthAction,
      forwardWeekAction,
      backwardWeekAction
    } = this.props;
    const { view } = this.state;

    const weekOfMonth =
      moment(currentDate).week() -
      moment(currentDate)
        .startOf("month")
        .week() +
      1;
    const month = monthAsString(currentDate.getMonth());

    switch (view) {
      case WEEKLY_VIEW:
        return (
          <React.Fragment>
            <Button
              onClick={() => {
                backwardWeekAction();
              }}
            >
              {"<"}
            </Button>
            <span className={classes.tableNavTitle}>{month}</span>
            <Button
              onClick={() => {
                forwardWeekAction();
              }}
            >
              {">"}
            </Button>
          </React.Fragment>
        );
      case MONTHLY_VIEW:
        return (
          <React.Fragment>
            <Button onClick={() => backwardMonthAction()}>{"<"}</Button>
            {month} <Button onClick={() => forwardMonthAction()}>{">"}</Button>
          </React.Fragment>
        );
      default:
        return null;
    }
  };
  render() {
    const {
      classes,
      students,
      getStudentsDataAction,
      markAttendanceAction,
      markAllAttendanceAction,
      unmarkAttendanceAction,
      unmarkAllAttendanceAction,
      addCommentAction,
      deleteCommentAction,
      clearAllAction,
      setCalendarDateAction,

      currentDate,
      activeDate
    } = this.props;

    const { view } = this.state;

    return (
      <div className={classes.root}>
        <Grid container justify="space-between">
          <Grid item md={9}>
            <div className={classes.tableTopItems}>
              <div className={classes.tableNavigation}>
                {this.renderTableNavigation()}
              </div>
              <div className={classes.viewButtons}>
                <Button
                  className={classes.button}
                  color={view === WEEKLY_VIEW ? "primary" : null}
                  variant="contained"
                  onClick={() => {
                    this.handleViewChange(WEEKLY_VIEW);
                  }}
                >
                  Weekly
                </Button>
                <Button
                  className={classes.button}
                  color={view === MONTHLY_VIEW ? "primary" : null}
                  variant="contained"
                  onClick={() => {
                    this.handleViewChange(MONTHLY_VIEW);
                  }}
                >
                  Monthly
                </Button>
              </div>
            </div>
            <Paper className={classes.tableWrapper}>
              <AttendanceTable
                view={view}
                students={students}
                getStudentsDataAction={getStudentsDataAction}
                markAttendanceAction={markAttendanceAction}
                markAllAttendanceAction={markAllAttendanceAction}
                unmarkAttendanceAction={unmarkAttendanceAction}
                unmarkAllAttendanceAction={unmarkAllAttendanceAction}
                addCommentAction={addCommentAction}
                deleteCommentAction={deleteCommentAction}
                clearAllAction={clearAllAction}
                setCalendarDateAction={setCalendarDateAction}
                currentDate={currentDate}
                activeDate={activeDate}
              />
            </Paper>
          </Grid>
          <Grid item md={3}>
            <Calendar
              value={currentDate}
              onChange={this.handleChangeDate}
              onClickDay={this.handleClickDay}
              className={classes.calender}
            />
          </Grid>
        </Grid>
      </div>
    );
  }
}

AttendancePage.propTypes = {
  classes: PropTypes.object.isRequired,
  students: PropTypes.array.isRequired,
  getStudentsDataAction: PropTypes.func.isRequired,
  markAttendanceAction: PropTypes.func.isRequired,
  markAllAttendanceAction: PropTypes.func.isRequired,
  unmarkAttendanceAction: PropTypes.func.isRequired,
  unmarkAllAttendanceAction: PropTypes.func.isRequired,
  addCommentAction: PropTypes.func.isRequired,
  deleteCommentAction: PropTypes.func.isRequired,
  setCalendarDateAction: PropTypes.func.isRequired,
  forwardMonthAction: PropTypes.func.isRequired,
  forwardWeekAction: PropTypes.func.isRequired,
  backwardMonthAction: PropTypes.func.isRequired,
  backwardWeekAction: PropTypes.func.isRequired,

  currentDate: PropTypes.object.isRequired,
  activeDate: PropTypes.object.isRequired
};

export default withStyles(styles)(AttendancePage);

import moment from "moment";
import { WEEKLY_VIEW, MONTHLY_VIEW } from "../constants/views";
export function getDates(startDate, view) {
  let aryDates = [];
  switch (view) {
    case WEEKLY_VIEW:
      {
        let date = moment(startDate)
          .subtract(startDate.getDay() - 1, "days")
          .toDate();
        for (let i = 0; i < 5; i++) {
          aryDates.push(date);
          date = moment(date)
            .add(1, "days")
            .toDate();
        }
      }
      break;
    case MONTHLY_VIEW:
      {
        let date = moment(startDate)
          .date(1)
          .toDate();
        let lastDate = moment(date)
          .add(1, "month")
          .subtract(1, "days")
          .toDate();
        while (Number(date) <= Number(lastDate)) {
          aryDates.push(date);
          date = moment(date)
            .add(1, "days")
            .toDate();
        }
      }
      break;
    default:
  }
  return aryDates.map(currentDate => ({
    string:
      dayAsString(currentDate.getDay()) +
      ", " +
      currentDate.getDate() +
      " " +
      monthAsString(currentDate.getMonth()) +
      " " +
      currentDate.getFullYear(),
    day: dayAsString(currentDate.getDay()),
    shortDay: shortDayAsString(currentDate.getDay()),
    date: currentDate.getDate(),
    dateObject: currentDate
  }));
}
export function monthAsString(monthIndex) {
  var month = [];
  month[0] = "January";
  month[1] = "February";
  month[2] = "March";
  month[3] = "April";
  month[4] = "May";
  month[5] = "June";
  month[6] = "July";
  month[7] = "August";
  month[8] = "September";
  month[9] = "October";
  month[10] = "November";
  month[11] = "December";

  return month[monthIndex];
}

export function dayAsString(dayIndex) {
  var weekdays = Array.from({ length: 7 });
  weekdays[0] = "Sunday";
  weekdays[1] = "Monday";
  weekdays[2] = "Tuesday";
  weekdays[3] = "Wednesday";
  weekdays[4] = "Thursday";
  weekdays[5] = "Friday";
  weekdays[6] = "Saturday";

  return weekdays[dayIndex];
}

export function shortDayAsString(dayIndex) {
  var weekdays = Array.from({ length: 7 });
  weekdays[0] = "Sun";
  weekdays[1] = "Mon";
  weekdays[2] = "Tue";
  weekdays[3] = "Wed";
  weekdays[4] = "Thurs";
  weekdays[5] = "Fri";
  weekdays[6] = "Sat";

  return weekdays[dayIndex];
}

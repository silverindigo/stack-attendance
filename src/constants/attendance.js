export const PRESENT = "PRESENT";
export const ABSENT = "ABSENT";
export const EARLY_DEPARTURE = "EARLY_DEPARTURE";
export const LATE = "LATE";

export const SYMBOLS = {
  PRESENT: "P",
  ABSENT: "A",
  EARLY_DEPARTURE: "ED",
  LATE: "L"
};

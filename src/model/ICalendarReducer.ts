export default interface ICalendarReducer {
  currentDate: Date;
  activeDate: Date;
}

import IAttendance from "./IAttendance";
import IComment from "./IComment";

export default interface IStudent {
  id: number;
  name: string;
  attendance: IAttendance;
  comments: IComment;
}

import IStudent from "./IStudent";

export default interface IStudentReducer {
  students: IStudent[];
}

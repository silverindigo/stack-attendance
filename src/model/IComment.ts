export default interface IComment {
  [date: string]: string;
}

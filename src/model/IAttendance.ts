export default interface IAttendance {
  [date: string]: string;
}

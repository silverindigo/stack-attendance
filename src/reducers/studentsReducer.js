import {
  GET_STUDENTS_DATA,
  MARK_ATTENDANCE,
  MARK_ALL_ATTENDANCE,
  UNMARK_ATTENDANCE,
  UNMARK_ALL_ATTENDANCE,
  ADD_COMMENT,
  DELETE_COMMENT
} from "../actions/types";
const initialState = {
  students: [] // array
};
export default (state = initialState, action) => {
  switch (action.type) {
    case GET_STUDENTS_DATA:
      return {
        students: action.payload // array
      };

    case MARK_ATTENDANCE: {
      const { studentId, date, attendanceType } = action.payload; // object
      const students = state.students.map(student => {
        if (student.id === studentId) {
          student = { ...student };
          student.attendance = {
            ...student.attendance,
            [date]: attendanceType
          };
        }
        return student;
      });

      return {
        students
      };
    }

    case MARK_ALL_ATTENDANCE: {
      const { date, attendanceType } = action.payload; // object
      const students = state.students.map(student => {
        student = { ...student };
        student.attendance = {
          ...student.attendance,
          [date]: attendanceType
        };
        return student;
      });

      return {
        students
      };
    }

    case UNMARK_ATTENDANCE: {
      const { studentId, date } = action.payload; // object
      const students = state.students.map(student => {
        if (student.id === studentId) {
          student = { ...student };
          student.attendance = {
            ...student.attendance
          };
          delete student.attendance[date];
        }
        return student;
      });

      return {
        students
      };
    }

    case UNMARK_ALL_ATTENDANCE: {
      const { date } = action.payload; // object
      const students = state.students.map(student => {
        student = { ...student };
        student.attendance = {
          ...student.attendance
        };
        delete student.attendance[date];
        return student;
      });

      return {
        students
      };
    }

    case ADD_COMMENT: {
      const { studentId, date, comment } = action.payload; // object
      const students = state.students.map(student => {
        if (student.id === studentId) {
          student = { ...student };
          student.comments = {
            ...student.comments,
            [date]: comment
          };
        }
        return student;
      });

      return {
        students
      };
    }
    case DELETE_COMMENT: {
      const { studentId, date } = action.payload; // object
      const students = state.students.map(student => {
        if (student.id === studentId) {
          student = { ...student };
          student.comments = {
            ...student.comments
          };
          delete student.comments[date];
        }
        return student;
      });

      return {
        students
      };
    }

    default:
      return state;
  }
};

import React, { Component } from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";

// theme
import { MuiThemeProvider } from "@material-ui/core/styles";
import theme from "./theme";

import "typeface-roboto";

// components
import AttendancePageContainer from "./containers/AttendancePageContainer";

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <MuiThemeProvider theme={theme}>
          <Switch>
            <Route exact path="/" component={AttendancePageContainer} />
          </Switch>
        </MuiThemeProvider>
      </BrowserRouter>
    );
  }
}

export default App;

import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import getStudentsDataAction from "../actions/getStudentsDataAction";
import markAttendanceAction from "../actions/markAttendanceAction";
import markAllAttendanceAction from "../actions/markAllAttendanceAction";
import unmarkAttendanceAction from "../actions/unmarkAttendanceAction";
import unmarkAllAttendanceAction from "../actions/unmarkAllAttendanceAction";
import addCommentAction from "../actions/addCommentAction";
import deleteCommentAction from "../actions/deleteCommentAction";

import forwardMonthAction from "../actions/forwardMonthAction";
import backwardMonthAction from "../actions/backwardMonthAction";
import forwardWeekAction from "../actions/forwardWeekAction";
import backwardWeekAction from "../actions/backwardWeekAction";
import setCalendarDateAction from "../actions/setCalendarDateAction";
import AttendancePage from "../components/AttendancePage";

class AttendancePageContainer extends Component {
  render() {
    const {
      students,
      getStudentsDataAction,
      markAttendanceAction,
      markAllAttendanceAction,
      unmarkAttendanceAction,
      unmarkAllAttendanceAction,
      addCommentAction,
      deleteCommentAction,
      setCalendarDateAction,
      forwardMonthAction,
      backwardMonthAction,
      forwardWeekAction,
      backwardWeekAction,

      currentDate,
      activeDate
    } = this.props;
    return (
      <AttendancePage
        students={students}
        getStudentsDataAction={getStudentsDataAction}
        markAttendanceAction={markAttendanceAction}
        markAllAttendanceAction={markAllAttendanceAction}
        unmarkAttendanceAction={unmarkAttendanceAction}
        unmarkAllAttendanceAction={unmarkAllAttendanceAction}
        addCommentAction={addCommentAction}
        deleteCommentAction={deleteCommentAction}
        setCalendarDateAction={setCalendarDateAction}
        forwardMonthAction={forwardMonthAction}
        backwardMonthAction={backwardMonthAction}
        forwardWeekAction={forwardWeekAction}
        backwardWeekAction={backwardWeekAction}
        currentDate={currentDate}
        activeDate={activeDate}
      />
    );
  }
}

AttendancePageContainer.propTypes = {
  students: PropTypes.array.isRequired,
  getStudentsDataAction: PropTypes.func.isRequired,
  markAttendanceAction: PropTypes.func.isRequired,
  markAllAttendanceAction: PropTypes.func.isRequired,
  unmarkAttendanceAction: PropTypes.func.isRequired,
  unmarkAllAttendanceAction: PropTypes.func.isRequired,
  addCommentAction: PropTypes.func.isRequired,
  deleteCommentAction: PropTypes.func.isRequired,
  setCalendarDateAction: PropTypes.func.isRequired,
  forwardMonthAction: PropTypes.func.isRequired,
  forwardWeekAction: PropTypes.func.isRequired,
  backwardMonthAction: PropTypes.func.isRequired,
  backwardWeekAction: PropTypes.func.isRequired,

  currentDate: PropTypes.object.isRequired,
  activeDate: PropTypes.object.isRequired
};

function mapStateToProps({ studentsReducer, calendarReducer }) {
  return {
    students: studentsReducer.students,
    activeDate: calendarReducer.activeDate,
    currentDate: calendarReducer.currentDate
  };
}

export default connect(
  mapStateToProps,
  {
    getStudentsDataAction,
    markAttendanceAction,
    markAllAttendanceAction,
    unmarkAttendanceAction,
    unmarkAllAttendanceAction,
    addCommentAction,
    deleteCommentAction,
    setCalendarDateAction,
    forwardMonthAction,
    forwardWeekAction,
    backwardMonthAction,
    backwardWeekAction
  }
)(AttendancePageContainer);

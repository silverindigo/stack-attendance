import { GET_STUDENTS_DATA } from "./types";

const studentsData = [
  {
    id: 1,
    name: "Angel Yu",
    attendance: {},
    comments: {}
  },
  {
    id: 2,
    name: "Benedict Wong",
    attendance: {},
    comments: {}
  },
  {
    id: 3,
    name: "Careen Yee",
    attendance: {},
    comments: {}
  },
  {
    id: 4,
    name: "Darren Ng",
    attendance: {},
    comments: {}
  },
  {
    id: 5,
    name: "Michael Yan",
    attendance: {},
    comments: {}
  }
];

export default () => dispatch => {
  dispatch({
    type: GET_STUDENTS_DATA,
    payload: studentsData
  });
};
